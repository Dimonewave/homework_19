﻿#include <iostream>

using namespace std;

class Animal {

public:

    virtual void Voice()
    {

        cout << "Voice of animal!\n";

    }
};

class Dog : public Animal {

public:

    void Voice() override
    {

        cout << "Woof!\n";

    }
};

class Cat : public Animal {

public:

    void Voice() override
    {

        cout << "Meow!\n";

    }
};

class Chicken : public Animal {

public:

    void Voice() override
    {

        cout << "Ko-ko-ko!\n";

    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Chicken;

    for (int i = 0; i < 3; i++) {
        animals[i]->Voice();
    }

    for (int i = 0; i < 3; i++) {
        delete animals[i];
    }

    return 0;
}
